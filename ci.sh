#!/bin/bash
set -e
set -x

#########
### vars

node_port=8080
app_name="jenkins-test"
test_app_name="test-$app_name"

## /vars
#########

##############
### functions

# Tests if an image has already been pulled.
function check_if_exists_and_pull {
  echo -e "Testing whether $1 image has been pulled? \n"

  if sudo docker images | grep -w $1
  then
    echo -e "$1 already exists. do not pull. \n"
  else
    echo -e "$1 does not exists. pulling now... \n"

    pull $1
  fi
}

# Tests if an image has already been built.
function check_if_exists_and_build {
  echo -e "Testing whether $1 image has been built? \n"

  if sudo docker images | grep -w $1
  then
    echo -e "$1 already exists. do not build. \n"
  else
    echo -e "$1 does not exists. building now... \n"

    build $1 $2
  fi
}

# Pulls a given image
function pull {
  echo -e "sudo docker pull $1"
  sudo docker pull $1 \
    | perl -pe '/Successfully pulled (\S+)/'
}

# Builds a given image
function build {
  rm -f docker-built-id
  echo -e "sudo docker build -t $1 $2"
  sudo docker build -t $1 $2 \
    | perl -pe '/Successfully built (\S+)/ && `echo -n $1 > docker-built-id`'
  if [ ! -f docker-built-id ]; then
    echo -e "No docker-built-id file found."
    exit 1
  else
    echo -e "docker-built-id file found, so build was successful."
  fi
  rm -f docker-built-id
}

## /functions
##############


##########################################
# Build or Pull the required docker images
##########################################

check_if_exists_and_pull "mammal/redis"
check_if_exists_and_pull "shykes/couchdb"
check_if_exists_and_build $app_name:$git_commit ./$app_name

rc=$?
if [[ $rc != 0 ]] ; then
    echo -e "Docker images build failed."
    exit $rc
fi

# we've built the images
echo -e "Docker images build passed successfully."

##########################################
# Run the docker images & execute npm test
##########################################

# kill any previously running test images
sudo docker kill redis couchdb $test_app_name | perl -pe '/Killed (\S+)/'
sudo docker rm redis couchdb $test_app_name | perl -pe '/Removed (\S+)/'

REDIS=$(sudo docker run --name redis -d mammal/redis)
COUCHDB=$(sudo docker run --name couchdb -d shykes/couchdb)

echo -e "Running tests... \n"

sudo docker run --name $test_app_name -p :$node_port -d --link redis:redis --link couchdb:couchdb  --entrypoint="./src/test.sh" -t $app_name:$git_commit

# Wait for the test container to exit
test_exit=$(sudo docker wait $test_app_name)

if [[ $test_exit != 0 ]] ; then
  echo -e "Tests failed inside docker."
  exit $test_exit
else
  echo -e "Tests passed inside docker."
fi

# if we get this far it'll be amaazing
echo -e "Tests passed successfully."



###################################################
# Run the docker images & serve it over a subdomain
###################################################
# kill any previously running test images
sudo docker kill redis couchdb | perl -pe '/Killed (\S+)/'
sudo docker rm redis couchdb | perl -pe '/Removed (\S+)/'

REDIS=$(sudo docker run --name redis -d mammal/redis)
COUCHDB=$(sudo docker run --name couchdb -d shykes/couchdb)

echo -e "Running tests... \n"

# this script must create the container_id & container_addr vars
container_id="`sudo docker run -p :$node_port -d -t --link redis:redis --link couchdb:couchdb $app_name:$git_commit`"
container_addr="`sudo docker port $container_id $node_port`"


# add it to hipache
redis-cli rpush frontend:$git_commit.$app_name.mammaldev.com $git_commit.$app_name
redis-cli rpush frontend:$git_commit.$app_name.mammaldev.com http://$container_addr
redis-cli lrange frontend:$git_commit.$app_name.mammaldev.com 0 -1

# email the new url?


# echo -e "Tests passed successfully. Pushing $app_name image to local private repository."

# docker tag $app_name localhost:5000/$app_name
# docker push localhost:5000/$app_name

# echo -e "Tested image pushed successfully to local repository."
