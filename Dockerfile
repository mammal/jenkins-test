FROM mammal/ubuntu:12.10

RUN apt-get update
RUN apt-get -y upgrade
RUN add-apt-repository ppa:chris-lea/node.js
RUN apt-get update
RUN apt-get -y install python g++ make nodejs

RUN echo 'export PATH="node_modules/.bin:$PATH"' >> /root/.bash_profile

# Bundle app source
ADD . /src

# Install app dependencies
RUN cd /src; npm install

EXPOSE 8080

CMD ["node", "/src/index.js"]
